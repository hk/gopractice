module myspider

go 1.15

require (
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20210410081132-afb366fc7cd1
	golang.org/x/text v0.3.6
	gopkg.in/olivere/elastic.v5 v5.0.86
)
