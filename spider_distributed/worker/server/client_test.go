package main

import (
	"fmt"
	"myspider/spider_distributed/config"
	"myspider/spider_distributed/rpcsupport"
	"myspider/spider_distributed/worker"
	"testing"
	"time"
)

func TestCrawlService(t *testing.T)  {
	const host=":9999"
	go rpcsupport.ServeRpc(host,worker.CrawlService{})
	time.Sleep(time.Second)

	client,err:=rpcsupport.NewClient(host)
	if err!=nil{
		panic(err)
	}
	req:=worker.Request{
		Url:"https://www.zhenai.com/zhenghun/beijing",
		Parser:worker.SerializedParser{
			Name:config.ParseCity,
			Args:"https://www.zhenai.com/zhenghun/beijing",
		},

	}

	var result worker.ParseResult
	err=client.Call(config.CrawlServiceRpc,req,&result)
	if err!=nil{
		t.Error(err)
	}else{
		for k,v:=range result.Items{
			fmt.Println(k,v.(map[string]interface{})["Url"])
		}

	}



}










