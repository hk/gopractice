package model

type Profile struct {
	Id        int
	Url 	  string
	Name      string
	Gender    string
	Age       int
	Height    int
	Weight    int
	Income    string
	Marriage  string
	Education string  //教育程度
	Position  string //地址
}
